# Contributing to `discretelogdb`

Create an account on https://gitlab.inria.fr.

Fork the project by clicking on the `Fork` item on the webpage
https://gitlab.inria.fr/dldb/discretelogdb. You must now have a copy of the
project `discretelogdb` in your space. Clone the project.

```
git clone git@gitlab.inria.fr:<user>/discretelogdb.git
```
or
```
git clone https://gitlab.inria.fr/<user>/discretelogdb.git
```

From now on, we assume that you are in the directory `discretelogdb`.

## Modifying or adding files

Use `request.py` as a pre-commit hook. It must be done to verify if `db.json`
is well formed and can be requested.

```
printf '#!/usr/bin/env bash\n\n./request.py -check\n' > .git/hooks/pre-commit
chmod 755 .git/hooks/pre-commit
```

### Adding a contribution

Eventually, you can create a branch where you put all your changes.

Modify files. Add the files, commit them and push them.

Records contain an array authors which contains concise name. It is a key which
refers to the authors element, at the end of the db.json file. If the author of
the record is this list, just use the key in the authors array. Otherwise, you
need first to create a key, add it in the array element.

### Sending a merge request

Look for the icon `+`, click on it and select `New merge request`.

Select the branch you want to merge to `dldb/discretelogdb`, and continue.

Describe your changes and submit the request.

## Keeping your fork up to date

### Adding remote from original repository

This must be done once.

```
git remote add upstream <what/the/way/you/want>dldb/discretelogdb.git
```

```
git fetch upstream
```

### Uptading your fork

Now, you can perform, at any time, an update of your fork.

```
git pull upstream master
```

## More about Gitlab

Additional help can be found by following [Gitlab
help](https://gitlab.inria.fr/help).

