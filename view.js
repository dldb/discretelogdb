var database;
var filter_keywords = ["all", "char", "ext"];
var filter_keywords_value = ["char", "ext"];
var sort_keywords = ["date", "size"];

function load_doc() {
  var xhttp;
  if (window.XMLHttpRequest) {
    // code for modern browsers
    xhttp = new XMLHttpRequest();
  } else {
    // code for IE6, IE5
    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      database = JSON.parse(this.responseText);
    }
  };
  xhttp.open("GET", "db.json", false);
  xhttp.send();
}

function do_on_submit() {
  var filter = document.getElementsByName("filter");
  var value = document.getElementsByName("value")[0].value;
  var sort = document.getElementsByName("sort");
  var query = "/?";
  if (filter[0].checked) {
    query += "filter=all";
  }
  else {
    if (filter[1].checked) {
      query += "filter=char";
    }
    else if (filter[2].checked) {
      query += "filter=ext";
    }
    query += "&value=" + value;
  }
  if (sort[0].checked) {
    query += "&sort=date";
  }
  else if (sort[1].checked) {
    query += "&sort=size";
  }
  history.pushState("", "", query); // TODO: must not be empty strings
}

function validate_url() {
  var urlParams = new URLSearchParams(window.location.search);
  var query = "/?";
  var is_filter_all = true;
  var is_sort_date = true;
  if (filter_keywords_value.includes(urlParams.get("filter")) && urlParams.has("value")) {
    query += "filter=" + urlParams.get("filter") + "&value=" + urlParams.get("value");
    is_filter_all = false;
  }
  if (is_filter_all) {
    query += "filter=all";
  }
  if (urlParams.get("sort") === "size") {
    query += "&sort=size";
    is_sort_date = false;
  }
  if (is_sort_date) {
    query += "&sort=date";
  }
  history.replaceState("", "", query); // TODO: must not be empty strings
}

function print_db_from_url() {
  validate_url();
  var urlParams = new URLSearchParams(window.location.search);
  var characteristic = null;
  var extension = null;
  var filter = document.getElementsByName("filter");
  var value = document.getElementsByName("value")[0];
  var sort = document.getElementsByName("sort");
  if (urlParams.get("filter") === "all") {
    document.getElementsByName("value")[0].value = "";
  }
  else {
    if (urlParams.get("filter") === "char") {
      characteristic = parseInt(urlParams.get("value"));
      filter[1].checked = true;
    }
    if (urlParams.get("filter") === "ext") {
      extension = parseInt(urlParams.get("value"));
      filter[2].checked = true;
    }
    value.value = urlParams.get("value");
  }
  if (urlParams.get("sort") === "date") {
    sort_date(database);
    document.getElementById("sort").innerHTML = "date";
    sort[0].checked = true;
  }
  if (urlParams.get("sort") === "size") {
    sort_size(database);
    document.getElementById("sort").innerHTML = "size";
    sort[1].checked = true;
  }
  print_db(database, characteristic, extension);
}

function onload() {
  load_doc();
  print_db_from_url();
}

function submit() {
  do_on_submit();
  print_db_from_url();
}

function erase_db() {
  var table = document.getElementById("db");
  var len = table.children.length;
  while (len > 1) {
    table.removeChild(table.children[1]);
    len = len - 1;
  }
}

function format_author(authors, key) {
  return authors[key].name[0] + " " + authors[key].name[1];
}

function print_db(database, characteristic, extension) {
  erase_db();
  //Length of the text in the tooltip
  var cut_tooltip = 155;
  //if n < 60, consider that p must be not printed, otherwise, print p
  var n_print_pn = 60;
  var table = document.getElementById("db");
  var row;
  var cell;
  var out;
  var att;
  var ind = 0;
  db = database.records
  authors = database.authors
  for (var i = 0; i < db.length; i++) {
    //Insert element in table or not
    if ((characteristic == null && extension == null) ||
      (characteristic == null && extension == parseInt(db[i].n)) ||
      (characteristic == parseInt(db[i].p) && extension == null)) {

      row = document.createElement("tr");
      //Type
      cell = document.createElement("td");
      if (parseInt(db[i].n) == 1) {
        cell.innerHTML = "<i>p</i>";
        //Not too long tooltip
        if (db[i].p.length <= cut_tooltip) {
          att = db[i].p;
        } else {
          att = db[i].p.substring(0, cut_tooltip - 1) + "…"
        }
        cell.setAttribute("title", att);
      } else if (parseInt(db[i].n) < n_print_pn) {
        cell.innerHTML = "<i>p<sup>" + db[i].n  + "</sup></i>";
        //Not too long tooltip
        if (db[i].p.length <= cut_tooltip) {
          att = db[i].p;
        } else {
          att = db[i].p.substring(0, cut_tooltip - 1) + "…"
        }
        cell.setAttribute("title", att);
      } else {
        cell.innerHTML = "<i>" + db[i].p + "<sup>" + db[i].n  + "</sup></i>";
      }
      row.appendChild(cell);
      // Size
      cell = document.createElement("td");
      cell.innerHTML = Math.ceil(Math.log2((parseFloat(db[i].p))) * parseFloat(db[i].n));
      row.appendChild(cell);
      // Date
      cell = document.createElement("td");
      cell.innerHTML = db[i].day + "/" + db[i].month + "/" + db[i].year;
      row.appendChild(cell);
      // Authors
      cell = document.createElement("td");
      out = ""
      out = format_author(authors, db[i].authors[0]);
      for (j = 1; j < db[i].authors.length - 1; j++) {
        out += ", " + format_author(authors, db[i].authors[j]);
      }
      if (db[i].authors.length > 1) {
        out += " and " + format_author(authors, db[i].authors[db[i].authors.length - 1]);
      }
      cell.innerHTML = out;
      row.appendChild(cell);
      //References
      cell = document.createElement("td");
      out = ""
      out = "<a href=\"" + db[i].references[0].link + "\">Link</a>";
      for (j = 1; j < db[i].references.length; j++) {
        out += ", " + "<a href=\"" + db[i].references[j].link + "\">link</a>";
      }
      cell.innerHTML = out;
      row.appendChild(cell);
      //Note
      cell = document.createElement("td");
      out = "";
      if (db[i].hasOwnProperty("notes")) {
        out = format_up(db[i].notes[0].note, 0);
        for (j = 1; j < db[i].notes.length; j++) {
          out += ", " + format_up(db[i].notes[j].note, j);
        }
      }
      out = format_http(out);
      if (is_prime(parseInt(db[i].n)) && parseInt(db[i].p) == 2) {
        if (out == "") {
          out += "Prime extension"
        } else {
          out += ", prime extension"
        }
      }
      cell.innerHTML = out;
      row.appendChild(cell);

      att = "w3-hover-grey";
      if (ind % 2 == 1) {
        att += " w3-light-grey";
      }
      ind = ind + 1;
      row.setAttribute("class", att);
      table.appendChild(row);
    }
  }
}

function format_up(s, j) {
  var string = s;
  if (s == "special p") {
    if (j == 0) {
      return "Special <i>p</i>";
    } else {
      return "special <i>p</i>";
    }
  }
  else if (j == 0) {
    string = s.charAt(0).toUpperCase() + s.slice(1);
  }

  return string;
}

function format_http(s) {
  var string = s;
  var hyperlink_beg = "<a href=\"";
  var hyperlink_end = "\">link</a>";
  var index_beg = string.indexOf("http");
  var index_end = -1;

  if (index_beg != -1) {
    string = string.slice(0, index_beg) + hyperlink_beg + string.slice(index_beg);
    index_end = index_beg + hyperlink_beg.length;
    while (string[index_end] !== " " && string[index_end] !== ")" && string[index_end] !== ",") {
      index_end++;
    }
    string = string.slice(0, index_end) + hyperlink_end + format_http(string.slice(index_end));
  }

  return string;
}

function errase_table(db) {
  table = document.getElementById("db");
  for (var i = db.length; i > 0; i--) {
    table.deleteRow(i);
  }
}

function sort_date(database) {
  database.records.sort(compare_size);
  database.records.sort(compare_date);
}

function sort_size(database) {
  database.records.sort(compare_date);
  database.records.sort(compare_size);
}

function compare_date(a, b) {
  var datea = new Date(a.year, a.month - 1, a.day);
  var dateb = new Date(b.year, b.month - 1, b.day);
  if (datea.getTime() < dateb.getTime()) {
    return 1;
  }
  if (datea.getTime() > dateb.getTime()) {
    return -1;
  }
  return 0;
}

function compare_p(a, b) {
  var pa = parseFloat(a.p);
  var pb = parseFloat(b.p);
  if (pa < pb) {
    return 1;
  }
  if (pa > pb) {
    return -1;
  }
  return 0;
}

function compare_n(a, b) {
  var na = parseInt(a.n);
  var nb = parseInt(b.n);
  if (na < nb) {
    return 1;
  }
  if (na > nb) {
    return -1;
  }
  return 0;
}

function compare_size(a, b) {
  var sa = Math.ceil(Math.log2((parseFloat(a.p))) * parseFloat(a.n));
  var sb = Math.ceil(Math.log2((parseFloat(b.p))) * parseFloat(b.n));
  if (sa < sb) {
    return 1;
  }
  if (sa > sb) {
    return -1;
  }
  return 0;
}

function is_prime(n) {
  if (n <= 1) {
    return false;
  }
  if (n <= 3) {
    return true;
  } else if (n % 2 == 0 || n % 3 == 0) {
    return false;
  }
  var i = 5;
  while (i * i <= n) {
    if (n % i == 0 || n % (i + 2) == 0) {
      return false;
    }
    i = i + 6;
  }
  return true;
}
