#! /usr/bin/python3

# Use this file as a precommit hook.
# Return values:
#   0: nothing to report
#   1: mandatory argument for a record is missing
#   2: json syntax error

# Call by ./request.py [-date] [-size] [-char <p>|-n <n>]

import sys
import json
import argparse
import traceback
import importlib.util
from math import log, ceil
from datetime import datetime
# Check if gmpy2 is available.
gmpy_available = importlib.util.find_spec("gmpy2")
if gmpy_available is not None:
    import gmpy2


def name_sorted(word):
    for i in range(len(word) - 1):
        if word[i] > word[i + 1]:
            return False
    return True


def key_date(i):
    if int(i["day"]) == 0 and int(i["month"]) == 0:
        # Date should be understood by datetime, fallback to 01/01/int(i["year"])
        return datetime(int(i["year"]), 1, 1)
    return datetime(int(i["year"]), int(i["month"]), int(i["day"]))


def key_size(i):
    return (ceil(int(i["n"]) * log(int(i["p"]), 2)))


def print_author(authors, key):
    return authors[key]["name"][0] + " " + authors[key]["name"][1]


# Define arguments.
parser = argparse.ArgumentParser(description='Request database.')
parser.add_argument('-date', help='Sort by date.', required=False, action='store_true')
parser.add_argument('-size', help='Sort by size.', required=False, action='store_true')
parser.add_argument('-char', help='Extract characteristic.', required=False)
parser.add_argument('-n', help='Extract extension.', required=False)
parser.add_argument('-check', help='Check db.json.', required=False, action='store_true')
parser.add_argument('-reverse', help='Reverse sorting.', required=False, action='store_true')
args = parser.parse_args()

# Process arguments.
date = args.date
size = args.size
char = "-1"
if args.char is not None:
    char = args.char
n = "-1"
if args.n is not None:
    if char != "-1":
        print("Can not extract characteristic and extension.")
        sys.exit(2)
    n = args.n
check = args.check
reverse = args.reverse

filer = open("db.json")

try:
    data = json.load(filer)
    records = data["records"]
    authors = data["authors"]

    if char != "-1":
        L = []
        for i in records:
            if i["p"] == char:
                L.append(i)
        records = L

    if n != "-1":
        L = []
        for i in records:
            if i["n"] == n:
                L.append(i)
        records = L

    if date:
        records.sort(key=key_date, reverse=reverse)
    if size:
        records.sort(key=key_date, reverse=reverse)
        records.sort(key=key_size, reverse=reverse)

    for i in records:
        s = ""
        # Bit size of p^n.
        s = s + str((ceil(int(i["n"]) * log(int(i["p"]), 2)))) + ", "

        # Extension.
        s = s + str(int(i["n"])) + ", "

        # Authors.
        s = s + "["
        for k in range(len(i["authors"]) - 1):
            s = s + print_author(authors, i["authors"][k]) + ", "
        s = s + print_author(authors, i["authors"][len(i["authors"]) - 1]) + "], "
        s = s + i["day"] + "/" + i["month"] + "/" + i["year"] + ", "

        # References.
        s = s + "["
        for k in range(len(i["references"]) - 1):
            s = s + i["references"][k]["link"] + ", "
        s = s + i["references"][len(i["references"]) - 1]["link"] + "]"

        # Notes.
        if "notes" in i.keys():
            s = s + ", " + "{"
            for k in range(len(i["notes"]) - 1):
                s = s + i["notes"][k]["note"] + ", "
            s = s + i["notes"][len(i["notes"]) - 1]["note"] + "}"

        # Print string or check data.
        if not check:
            print(s)
        else:
            # Check primality of p.
            if gmpy_available is not None:
                if not gmpy2.is_prime(gmpy2.mpz(i["p"], 10)):
                    print(f'Characteristic {i["p"]} is not prime.')
                    filer.close()
                    sys.exit(3)

            # Check if n is positive.
            if not int(i["n"]) >= 1:
                print(f'Extension {i["n"]} is not valid.')
                filer.close()
                sys.exit(3)

            # Format date
            if len(i["day"]) != 2:
                print(f'Format day {i["day"]} with two digits.')
                filer.close()
                sys.exit(3)
            if len(i["month"]) != 2:
                print(f'Format month {i["month"]} with two digits.')
                filer.close()
                sys.exit(3)
            if len(i["year"]) != 4:
                print(f'Format year {i["year"]} with four digits.')
                filer.close()
                sys.exit(3)
            # Check if date exists.
            try:
                if int(i["day"]) != 0 or int(i["month"]) != 0:
                    datetime.strptime(f'{i["day"]}{i["month"]}{i["year"]}', "%d%m%Y")
            except ValueError:
                print(f'Date {i["day"]}/{i["month"]}/{i["year"]} is not valid.')
                filer.close()
                sys.exit(3)

# Semantic error in db.json.
except json.decoder.JSONDecodeError as e:
    print(str(e))
    filer.close()
    sys.exit(2)

# Other error.
except Exception:
    print(traceback.format_exc())
    filer.close()
    sys.exit(1)

filer.close()
sys.exit(0)
