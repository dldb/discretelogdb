# Database of computations of discrete logarithms

## Aim of the project

The aim of this project is to list all the computations of discrete logarithms
in finite fields of any characteristic (with an individual logarithm
computation of a given target in the considered finite field), with
references.

For now, the presentation website can be found [here](https://dldb.loria.fr/).

## List of main files

* `db.json`: the database, JSON format.
* `request.py`: a python script to perform simple requests on `db.json`.
* `index.html` and `view.js`: example of a presentation tool on the web.

## Contributing to the project

`discretelogdb` welcomes contributions. See [CONTRIBUTING.md](CONTRIBUTING.md).

## How to cite

Laurent Grémy and Aurore Guillevic. *DiscreteLogDB, a database of computations
of discrete logarithms*, 2017, `https://gitlab.inria.fr/dldb/discretelogdb`.

```
@misc{dldb,
    author={Laurent Gr\'{e}my and Aurore Guillevic},
    title={{DiscreteLogDB}, a database of computations of discrete logarithms},
    year={2017},
    note={\url{https://gitlab.inria.fr/dldb/discretelogdb}}
}
```

## Contributors

* Pierrick Gaudry
* Laurent Gr&eacute;my
* Aurore Guillevic

## Licence

This project is distributed under the [Creative Commons Zero
license](LICENSE).

## Other lists of computations of discrete logarithms

* [Discrete logarithm records on Wikipedia](https://en.wikipedia.org/wiki/Discrete_logarithm_records)
* [Slides of Razvan Barbulescu, ECC 2014](http://www.imsc.res.in/~ecc14/slides/razvan.pdf)
* [Slides of Jens Zumbr&auml;gel, ECC 2014](http://www.imsc.res.in/~ecc14/slides/jens.pdf)
* [Indiscreet logarithms in finite fields of small characteristic, by Robert Granger, Thorsten Kleinjung and Jens Zumbr&auml;gel](https://arxiv.org/abs/1604.03837v1)
* [C&eacute;cile Pierrot's thesis](https://almasty.lip6.fr/~pierrot/papers/Cecile'sPhDThesis.pdf)
